#!/bin/bash
set -e

# Download sources
mkdir -p SOURCES
spectool -g -C ./SOURCES SPECS/xdma-dkms.spec
# Build RPM
rpmbuild -v --define "%_topdir `pwd`" -bb SPECS/xdma-dkms.spec

tree RPMS

# Remove unwanted RPM
rm -f RPMS/x86_64/*-debuginfo-*.rpm
